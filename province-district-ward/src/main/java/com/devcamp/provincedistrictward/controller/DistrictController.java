package com.devcamp.provincedistrictward.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.provincedistrictward.model.District;
import com.devcamp.provincedistrictward.model.Province;
import com.devcamp.provincedistrictward.model.Ward;
import com.devcamp.provincedistrictward.repository.IDistrictRepository;
import com.devcamp.provincedistrictward.repository.IProvinceRepository;
import com.devcamp.provincedistrictward.service.DistrictService;
import com.devcamp.provincedistrictward.service.ProvinceService;
import com.devcamp.provincedistrictward.service.WardService;

@RestController
@RequestMapping("/district")
@CrossOrigin
public class DistrictController {
  @Autowired
  DistrictService districtService;
  @Autowired
  ProvinceService provinceService;
  @Autowired
  IDistrictRepository pIDistrictRepository;
  @Autowired
  IProvinceRepository pIProvinceRepository;

  @GetMapping("/all")
  public ResponseEntity<List<District>> getAllDistricts() {
    try {
      return new ResponseEntity<>(districtService.getAllDistricts(),
          HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // get district by province id
  @GetMapping("/search/provinceId={id}")
  public ResponseEntity<Set<District>> getDistrictByProvinceId(@PathVariable(name = "id") int id) {
    try {
      Set<District> vDistrict = provinceService.getDistrictByProvinceId(id);
      if (vDistrict != null) {
        return new ResponseEntity<>(vDistrict, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // get district by id
  @GetMapping("/detail/{id}")
  public ResponseEntity<Object> getDistrictById(@PathVariable(name = "id", required = true) Integer id) {
    Optional<District> district = pIDistrictRepository.findById(id);
    if (district.isPresent()) {
      return new ResponseEntity<>(district, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // create new district by province
  @PostMapping("/create/{id}")
  public ResponseEntity<Object> createDistrict(@PathVariable("id") int id, @RequestBody District pDistrict) {
    try {
      Province province = pIProvinceRepository.findById(id);
      if (province != null) {
        District newDistrict = new District();
        newDistrict.setNameDistrict(pDistrict.getNameDistrict());
        newDistrict.setPrefixDistrict(pDistrict.getPrefixDistrict());
        newDistrict.setProvince(province);
        District _district = pIDistrictRepository.save(newDistrict);
        // District _district = districtRepository.save(pDistrict);
        return new ResponseEntity<>(_district, HttpStatus.CREATED);
      } else
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // update distict
  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateDistrict(@PathVariable(name = "id") Integer id,
      @RequestBody District updateDistrict) {
    Optional<District> district = pIDistrictRepository.findById(id);
    try {
      if (district.isPresent()) {
        District _district = district.get();
        _district.setNameDistrict(updateDistrict.getNameDistrict());
        _district.setPrefixDistrict(updateDistrict.getPrefixDistrict());
        _district.setProvince(updateDistrict.getProvince());
        _district.setWards(updateDistrict.getWards());

        return new ResponseEntity<>(pIDistrictRepository.save(_district),
            HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // delete district by Id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<District> deleteDistrictById(@PathVariable("id") Integer id) {
    try {
      pIDistrictRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
