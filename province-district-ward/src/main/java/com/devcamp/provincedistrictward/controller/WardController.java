package com.devcamp.provincedistrictward.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.provincedistrictward.model.District;
import com.devcamp.provincedistrictward.model.Ward;
import com.devcamp.provincedistrictward.repository.IDistrictRepository;
import com.devcamp.provincedistrictward.repository.IWardRepository;
import com.devcamp.provincedistrictward.service.DistrictService;
import com.devcamp.provincedistrictward.service.WardService;

@RestController
@RequestMapping("/ward")
@CrossOrigin
public class WardController {
  @Autowired
  WardService wardService;
  @Autowired
  DistrictService districtService;
  @Autowired
  IWardRepository pIWardRepository;
  @Autowired
  IDistrictRepository pDistrictRepository;

  @GetMapping("/all")
  public ResponseEntity<List<Ward>> getAllWards() {
    try {
      return new ResponseEntity<>(wardService.getAllWards(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // get ward by district id
  @GetMapping("/search/districtId={id}")
  public ResponseEntity<Set<Ward>> getWardByDistrictId(@PathVariable(name = "id") int id) {
    try {
      Set<Ward> vWards = districtService.getWardByDistrictId(id);
      if (vWards != null) {
        return new ResponseEntity<>(vWards, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // get ward by id
  @GetMapping("/detail/{id}")
  public Ward getWardById(@PathVariable(name = "id") Integer id) {
    if (pIWardRepository.findById(id).isPresent())
      return pIWardRepository.findById(id).get();
    else
      return null;
  }

  // create new ward with district id
  @PostMapping("/create/{id}")
  public ResponseEntity<Object> createWard(@PathVariable("id") int id, @RequestBody Ward pWard) {
    try {
      District district = pDistrictRepository.findById(id);
      if (district != null) {
        Ward newWard = new Ward();
        newWard.setNameWard(pWard.getNameWard());
        newWard.setPrefixWard(pWard.getPrefixWard());
        newWard.setDistrict(district);
        Ward _ward = pIWardRepository.save(newWard);
        // Ward _ward = wardRepository.save(pWard);
        return new ResponseEntity<>(_ward, HttpStatus.CREATED);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // update ward
  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateWard(@PathVariable(name = "id") Integer id,
      @RequestBody Ward updateWard) {
    Optional<Ward> ward = pIWardRepository.findById(id);
    try {
      if (ward.isPresent()) {
        Ward _ward = ward.get();
        _ward.setNameWard(updateWard.getNameWard());
        _ward.setPrefixWard(updateWard.getPrefixWard());
        _ward.setDistrict(updateWard.getDistrict());

        return new ResponseEntity<>(pIWardRepository.save(_ward),
            HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // delete ward by Id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Ward> deleteWardById(@PathVariable("id") Integer id) {
    try {
      pIWardRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
