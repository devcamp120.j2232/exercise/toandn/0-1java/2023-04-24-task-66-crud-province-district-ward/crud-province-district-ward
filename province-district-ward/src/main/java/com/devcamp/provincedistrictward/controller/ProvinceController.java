package com.devcamp.provincedistrictward.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.support.Repositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.provincedistrictward.model.District;
import com.devcamp.provincedistrictward.model.Province;
import com.devcamp.provincedistrictward.repository.IProvinceRepository;
import com.devcamp.provincedistrictward.service.DistrictService;
import com.devcamp.provincedistrictward.service.ProvinceService;

@RestController
@RequestMapping("/province")
@CrossOrigin
public class ProvinceController {
  @Autowired
  ProvinceService provinceService;

  @Autowired
  IProvinceRepository pIProvinceRepository;

  @Autowired
  DistrictService districtService;

  @GetMapping("/all")
  public ResponseEntity<List<Province>> getAllProvinces() {
    try {
      return new ResponseEntity<>(provinceService.getAllProvinces(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  // get province by id
  @GetMapping("/detail/{id}")
  public ResponseEntity<Object> getProvinceById(@PathVariable(name = "id", required = true) Integer id) {
    Optional<Province> province = pIProvinceRepository.findById(id);
    if (province.isPresent()) {
      return new ResponseEntity<>(province, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/province5")
  public ResponseEntity<List<Province>> getFiveProvince(
      @RequestParam(value = "page", defaultValue = "1") String page,
      @RequestParam(value = "size", defaultValue = "5") String size) {
    try {
      Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page),
          Integer.parseInt(size));
      List<Province> list = new ArrayList<Province>();
      pIProvinceRepository.findAll(pageWithFiveElements).forEach(list::add);
      return new ResponseEntity<>(list, HttpStatus.OK);
    } catch (Exception e) {
      return null;
    }
  }

  // create new province
  @PostMapping("/create")
  public ResponseEntity<Object> createProvince(@RequestBody Province newProvince) {
    try {
      Province _newProvince = pIProvinceRepository.save(newProvince);
      return new ResponseEntity<>(_newProvince, HttpStatus.CREATED);

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // update province
  @PutMapping("/update/{id}")
  public ResponseEntity<Object> updateProvince(@PathVariable(name = "id") Integer id,
      @RequestBody Province updateProvince) {
    Optional<Province> province = pIProvinceRepository.findById(id);
    try {
      if (province.isPresent()) {
        Province _province = province.get();
        _province.setCodeProvince(updateProvince.getCodeProvince());
        _province.setNameProvince(updateProvince.getNameProvince());
        _province.setDistricts(updateProvince.getDistricts());

        return new ResponseEntity<>(pIProvinceRepository.save(_province),
            HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }

    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // delete province by Id
  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Province> deleteProvinceById(@PathVariable("id") Integer id) {
    try {
      pIProvinceRepository.deleteById(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      System.out.println(e);
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  // get province by id district
  @CrossOrigin
  @GetMapping("/DistrictId/{id}")
  public Province getProvinceByDistrictId(@PathVariable("id") Integer id) {
    return pIProvinceRepository.findByDistrictsId(id);
  }
}