package com.devcamp.provincedistrictward.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.provincedistrictward.model.District;
import com.devcamp.provincedistrictward.model.Ward;
import com.devcamp.provincedistrictward.repository.IDistrictRepository;

@Service
public class DistrictService {
  @Autowired
  IDistrictRepository pIDistrictRepository;

  public ArrayList<District> getAllDistricts() {
    ArrayList<District> districts = new ArrayList<>();
    pIDistrictRepository.findAll().forEach(districts::add);
    return districts;
  }

  public Set<Ward> getWardByDistrictId(int id) {
    District vDistrict = pIDistrictRepository.findById(id);
    if (vDistrict != null) {
      return vDistrict.getWards();
    } else {
      return null;
    }
  }
}
