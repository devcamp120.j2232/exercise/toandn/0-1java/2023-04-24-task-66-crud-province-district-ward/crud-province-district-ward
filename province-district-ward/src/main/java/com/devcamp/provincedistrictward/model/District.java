package com.devcamp.provincedistrictward.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "districts")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class District {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "name_district")
  private String nameDistrict;

  @Column(name = "prefix_district")
  private String prefixDistrict;

  @OneToMany(targetEntity = Ward.class, cascade = CascadeType.ALL)
  @JoinColumn(name = "district_id")
  private Set<Ward> wards;

  @ManyToOne
  @JsonIgnore
  private Province province;

  public District() {
  }

  public District(int id, String nameDistrict, String prefixDistrict) {
    this.id = id;
    this.nameDistrict = nameDistrict;
    this.prefixDistrict = prefixDistrict;
  }

  public District(int id, String nameDistrict, String prefixDistrict, Province province) {
    this.id = id;
    this.nameDistrict = nameDistrict;
    this.prefixDistrict = prefixDistrict;
    this.province = province;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNameDistrict() {
    return nameDistrict;
  }

  public void setNameDistrict(String nameDistrict) {
    this.nameDistrict = nameDistrict;
  }

  public String getPrefixDistrict() {
    return prefixDistrict;
  }

  public void setPrefixDistrict(String prefixDistrict) {
    this.prefixDistrict = prefixDistrict;
  }

  public Province getProvince() {
    return province;
  }

  public void setProvince(Province province) {
    this.province = province;
  }

  public Set<Ward> getWards() {
    return wards;
  }

  public void setWards(Set<Ward> wards) {
    this.wards = wards;
  }
}
