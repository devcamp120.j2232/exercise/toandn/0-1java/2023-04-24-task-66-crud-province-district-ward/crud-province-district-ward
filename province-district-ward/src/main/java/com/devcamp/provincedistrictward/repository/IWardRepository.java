package com.devcamp.provincedistrictward.repository;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provincedistrictward.model.Ward;

public interface IWardRepository extends JpaRepository<Ward, Integer> {
  Ward findById(int id);
  // List<Ward> findByProvinceId(int id);

  // List<Ward> findByDistrictId(int id);
}
