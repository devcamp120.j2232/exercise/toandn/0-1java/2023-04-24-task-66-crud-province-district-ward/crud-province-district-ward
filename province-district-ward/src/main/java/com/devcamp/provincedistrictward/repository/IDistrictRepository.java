package com.devcamp.provincedistrictward.repository;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.provincedistrictward.model.District;

public interface IDistrictRepository extends JpaRepository<District, Integer> {
  District findById(int Id);

  List<District> findByProvinceId(int Id);
}
